# raspbian-hmi



## CSC 7575 Cyber-Physical Systems Security Term Project
This is part of Daniel Simpson and Kaitlyn Cottrell's term project for CSC-7575. This HMI will interface with our raspberry pi-based physical system to control it and report on its status.

Since this uses the RPi.GPIO package, it will not run outside of a raspbian environment.

