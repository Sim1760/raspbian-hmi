# Term Project AGV GUI / HMI v1
# Daniel Simpson
# 3/22/23
# CSC-7575
# Note: due to the use of RPi.GPIO this will only run on a raspberry pi.

# libraries
import tkinter as tk
from tkinter import *
import RPi.GPIO as GPIO
from threading import Thread

# initialize GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False) # turn off warnings for this application
GPIO.setup(22, GPIO.OUT, initial=GPIO.HIGH) # motor, inverts so start True
GPIO.setup(16, GPIO.OUT, initial=GPIO.LOW) # green LED, motor starts off so start False
GPIO.setup(18, GPIO.OUT, initial=GPIO.HIGH) # red LED, motor starts off so start True

# window is an instance of tkinter's Tk class
window = tk.Tk()

# photoimage on the window
window.geometry("899x599") # size of the image
bg = PhotoImage(file="images/control_panel2.png")
bg_image_label = Label(window, image=bg)
# no resize to keep centered on picture
window.resizable(False, False)
# make window borderless to force use of safe closeout button
window.overrideredirect(True)
bg_image_label.place(x=0, y=0) # place background

# define images for the LEDs on/off states
red_on = PhotoImage(file="images/red_on.png")
red_off = PhotoImage(file="images/red_off.png")
green_on = PhotoImage(file="images/green_on.png")
green_off = PhotoImage(file="images/green_off.png")
close_img = PhotoImage(file="images/close.png")

# define labels for green and red leds
green_label = Label(window,
        image=green_off,
        highlightthickness=0
        )
red_label = Label(window,
        image=red_on,
        highlightthickness=0
        )

# place g/r LED label widgets
green_label.place(x=661, y=150)
red_label.place(x=765, y=150)

# keep track of light states
global red_status 
red_status = True      # starts True bc machine not running
global green_status 
green_status = False   # starts false bc machine not running
global pin22
pin22 = 0 # init
global pin18
pin18 = 0 # init
global pin16
pin16 = 0 # init
greetingText = "Hello AGV Operator!\nPin 22 Motor:         " + str(pin22) + "\nPin 18 Red LED:     " + str(pin18) + "\nPin 16 Green LED:  " + str(pin16) 
# flag variable for thread execution, to be accessed globally
threadExecute = True

# define toggle function for LEDs, motor
def toggle():
    global red_status
    global green_status

    # determine status and set lights per button press
    if red_status:
        red_label.config(image=red_off) # if red on change to off
        red_status = False
        GPIO.output(18, False) # send false to pin 18
    else:
        red_label.config(image=red_on) # if not on, turn on
        red_status = True
        GPIO.output(18, True) # send true to pin 18
    if green_status:
        green_label.config(image=green_off) # if green on turn off
        green_status = False
        GPIO.output(16, False) # send false to pin 16
        GPIO.output(22, True) # turn motor off when green is off
    else:
        green_label.config(image=green_on) # if green off turn on
        green_status = True
        GPIO.output(16, True) # send true to pin 16
        GPIO.output(22, False) # turn motor on when green is on

# define pin query function
def pinquery():
    GPIO.setmode(GPIO.BOARD)
    global pin22
    global pin18
    global pin16
    global greetingText
    pin22 = GPIO.input(22) # but this will need to be inverted bc faulty wiring
    pin18 = GPIO.input(18)
    pin16 = GPIO.input(16)
    # invert pin22
    if pin22 == 1:
       pin22 = 0
    else:
       pin22 = 1
    # need to use this information to update the message
    greetingText = "Hello AGV Operator!\nPin 22 Motor:         " + str(pin22) + "\nPin 18 Red LED:     " + str(pin18) + "\nPin 16 Green LED:  " + str(pin16) 


# define update screen function to loop and update the screen
def update_screen():
    global threadExecute # access thread execution flag globally
    global greetingText  # need to be able to update this outside the function
    while threadExecute:
        # update greeting text to get new readings
        pinquery()
        greeting.config(text=greetingText)
        window.update() # force the new text to display

# define close function
def close():
    global threadExecute
    threadExecute = False   # kill loop in thread
    GPIO.cleanup()          # clear pins
    window.quit()           # close

# tk.label class Label for text on screen
greeting = tk.Label(text=greetingText, 
        bg="black", 
        fg="green",
        width=25,
        height=5,
        font=(25)
        )

# attach label to window with .place() method
greeting.place(x=330, y=70)

# AGV on/off button
button = tk.Button(
        text="AGV On\Off",
        width=15,
        height=2,
        bg="red",
        fg="white",
        border=5,
        relief=RAISED,
        command=toggle
)

# custom closeout button (red X)
quitButton = tk.Button(
        window,
        image=close_img,
        command=close
        )

# attach buttons to window
button.place(x=680, y=300)
quitButton.place(x=870, y=0)

# spawn thread to keep up with screen information here
thread = Thread(target=update_screen)
thread.start()

# keep this at the end to make the GUI a loop
window.mainloop()
